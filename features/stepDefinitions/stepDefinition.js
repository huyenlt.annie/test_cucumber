const { Given, When, Then } = require('cucumber');
const LoginPage = require("../../pages/client/LoginPage");
const RegisterPage = require('../../pages/client/RegisterPage');
const CreateAccoutPage = require('../../pages/client/CreateAccountPage');
const ServiceLinesPage = require("../../pages/client/ServiceLinesPage");
const QuestionBankPage = require("../../pages/client/QuestionBankPage");
const FeelingPage = require("../../pages/client/FeelingPage");
const ResultPage = require("../../pages/client/ResultPage");
const SelectPlanPage = require("../../pages/client/SelectPlanPage");
const PaymentPage = require("../../pages/client/PaymentPage");
const UpgradePage = require("../../pages/client/UpgradePage");
const ShippingPage = require("../../pages/client/ShippingPage");
const ContactPage = require("../../pages/client/ContactPage");
const WeightHeightPage = require("../../pages/client/WeightHeightPage");
const PronounPage = require('../../pages/client/PronounPage');
const AdminLoginPage = require('../../pages/cerabral_admin/AdminLoginPage');
const HomePageAdmin = require('../../pages/cerabral_admin/HomePageAdmin');
const PatientsPageAdmin = require('../../pages/cerabral_admin/PatientsPageAdmin');
const CareCounselorPage = require('../../pages/client/CareCounselorPage');
const BoxChatMessagePage = require("../../pages/client/BoxChatMessagePage");


Given('I navigate to login page from the landing page', async function () {
    await LoginPage.navigateToLink("https://app-fe-release.getcerebral.com/");
});

When('I click on {string}', async function (string) {
    await LoginPage.clickResgisterButton();
});

When('complete account information step', async function () {
    await RegisterPage.typeTextFromRegister();
});

When('completed personal information step', async function () {
    await CreateAccoutPage.createAccount();
});

When('select {string} service line', async function (string) {
    await ServiceLinesPage.clickCheckBoxServiceLines(string);
});

When('answer a list of questions regarding to anxiety', async function () {
    await ServiceLinesPage.clickConfirmButton();
    await QuestionBankPage.informationScreening(); //next button
    //sua lai next
    // await ShippingPage.clickNextButton();
    // await ShippingPage.clickNextButton();
    // await ShippingPage.clickNextButton();
});

When('answer a list of questions regarding to alcohol', async function () {
    await FeelingPage.clickRandomOptionFeeling();
});


Then('I see Preliminary Screen Indicators screen showing my “Anxiety“ level and “Alcohol use“ level', async function () {
    await FeelingPage.clickCheckBoxFeeling();
});

When('I click “Confirm“', async function () {
    await FeelingPage.clickConfirmButton();
    await ResultPage.clickContinueButton();
    await ResultPage.clickNoButton();

});

Then('I see “Medication & Care Counseling“ plan', async function () {
    await SelectPlanPage.verifyMedicationTitle();

});

When('I click “VIEW MORE PLANS“', async function () {
    await SelectPlanPage.clickViewMorePlans();
});

Then('I see “Medication & Care Counseling“, “Medication & Therapy“, “Therapy“ plans', async function () {
    await SelectPlanPage.verifyPlanTitle();
});

When('I note down the first month price and discount price of “Medication & Care Counseling“ plan', async function () {
    await SelectPlanPage.notePriceDiscount();
});


When('select “Medication & Care Counseling“ plan', async function () {
    await SelectPlanPage.selectMedicationCareCounseling();
});

When('get navigated to Billing Information screen', async function () {
    await SelectPlanPage.verifyNavigateBilling();
});

Then('I see “Medication & Care Counseling“ which I chose', async function () {
    await PaymentPage.checkedCouselingNote();

});

Then('the price for first month is like I noted', async function () {
    await PaymentPage.checkedPriceNote();
});


Then('the price for month later is ${int}', async function (int) {
    await PaymentPage.checkedPriceMonthAfter(); //85-> sai
});


Then('the promo code is “CARE30“', async function () {
    await PaymentPage.checkedCare30();
});

When('I complete check out', async function () {
    await PaymentPage.checkOut();
});


Then('I see {string} plan', async function (string) {
    await UpgradePage.verifyAddTherapyTitle();
});

When('I click {string} to skip', async function (string) {
    await UpgradePage.clickContinueWithCurrent();
    await UpgradePage.clickRandomPerform();
    await ResultPage.clickNoButton();
});

When('complete shipping information', async function () {
    await ShippingPage.typeTextShipping();
    await ShippingPage.clickNextButton();
    await ResultPage.clickNoButton();
});

When('complete delivery method', async function () {
    await ContactPage.createContact();
    await ShippingPage.clickNextButton();
});


When('complete post checkout questions', async function () {
    await UpgradePage.clickRandomPerform();
    await PronounPage.clickCheckBoxDiagnosed();
    await FeelingPage.clickConfirmButton();
    await ResultPage.clickNoButton();
    await WeightHeightPage.typeTextWeightHeight();
    await FeelingPage.clickCheckBoxFeeling();
    await FeelingPage.clickConfirmButton();

    await PronounPage.clickSex();//sex
    await FeelingPage.clickRandomOptionFeeling();

    await PronounPage.clickCheckBoxDiagnosed();
    await FeelingPage.clickConfirmButton();
    await ResultPage.clickNoButton();
    await ResultPage.clickNoButton();
    await ResultPage.clickNoButton();

    await UpgradePage.typeTextTreatMent();
    // await ShippingPage.clickNextButton();


});


Given('complete “Identity Verification“', async function () {
    await AdminLoginPage.navigateToLinkAdmin();
    await AdminLoginPage.typeTextLoginAdmin();
    await HomePageAdmin.searchToCutomers();
    await PatientsPageAdmin.clickVerifycationComplete(); //close admin

});

When('select “I prefer a video visit“', async function () {

    await LoginPage.openWindowClient("https://app-fe-release.getcerebral.com/");
    await LoginPage.typeTextLogin();

    await QuestionBankPage.clickContinueButton();
    await QuestionBankPage.informationScreening();//->button Next
    await QuestionBankPage.communicationPreference();//-> video
    await QuestionBankPage.informationScreening();//->button Next
});


When('schedule appointment with prescriber', async function () {
    await QuestionBankPage.appoiment();
    await QuestionBankPage.informationScreening();//->button Next (2 LAN)
    // await QuestionBankPage.clickNext();
    await QuestionBankPage.clickContinueButton();

});

When('select Counselor preferences', async function () {
    await QuestionBankPage.informationScreening();
});


When('schedule appointment with counselor', async function () {
    await QuestionBankPage.appoiment();

});


When('click “Continue to the client portal“', async function () {
    await CareCounselorPage.clickContinueToTheClient();

});


Then('I get navigated to Message box chat', async function () {
    await BoxChatMessagePage.verifyNciCounselor();
});

Then('see Welcome Message from care counselor', async function () {
    await BoxChatMessagePage.welcomeMessage();
});

