import { Selector } from 'testcafe';

fixture`TestCafe`
    .page('https://devexpress.github.io/testcafe/');

test('Switch to a different window', async t => {
    const homepage = await t.getCurrentWindow();
    const documentation = await t.openWindow('http://devexpress.github.io/testcafe/documentation');
    await t.switchToWindow(homepage)
        .switchToWindow(documentation);
});